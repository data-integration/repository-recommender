# Repository Recommender

This repository contains the recommender system and API implementation that provides repository recommendations.

## Development

Setup a new virtual envirnoment:

```shell
$ virtualenv .venv
$ . .venv/bin/activate
$ pip install -r requirements.txt
```

Initialized the application:

```shell
$ python docker_entrypoint.py
```

Running on a unix machine:

```shell
$ . .venv/bin/activate
$ pip install gunicorn
$ gunicorn --reload repository_recommender.app
```

Running on windows:

```shell
$ pip install waitress
$ waitress-serve --port=8000 repository_recommender.app:api
```

## Installation

As a prerequisite for setting up the recommender a PostgreSQL and a Redis database is needed. In the [infrastructure repository](https://gitlab.com/data-integration/infrastructure) more details on how this could be done using Docker can be found. To start a Docker container running the recommender the provided `docker-compose.yml` file can be used:

```sh
$ docker-compose up -d
```

The connection details for the databases can be provided using environment variables:

* `BACKEND_NET`: The name of a Docker network with access to the databases.
* `REVERSE_PROXY_NET`: The name of a Docker network connected to a reverse proxy that routes to the recommender API listening on port 8080.
* `POSTGRES_HOST`: The PostgreSQL host.
* `POSTGRES_DB`: The PostgreSQL database.
* `POSTGRES_USER`: The PostgreSQL user.
* `POSTGRES_PASSWORD`: The PostgreSQL password.
* `REDIS_HOST`: The Redis host.
