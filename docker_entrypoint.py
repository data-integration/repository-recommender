#!/usr/bin/env python
import logging
import os
import sys

import pandas as pd
import psycopg2
import redis as redis
from nearpy import Engine
from nearpy.hashes import RandomBinaryProjections
from nearpy.storage import RedisStorage
from sqlalchemy import create_engine

import repository_recommender.config as config

# Connect to database
db_connection = psycopg2.connect(
    host=config.DB_HOST,
    port=config.DB_PORT,
    dbname=config.DB_NAME,
    user=config.DB_USER,
    password=config.DB_PASSWORD
)
db_connection_streams = create_engine(
    config.DB_URL,
    execution_options=dict(stream_results=True)
)

# Connect to redis cache
redis = redis.Redis(
    host=config.REDIS_HOST,
    port=config.REDIS_PORT,
    db=config.REDIS_DB
)
redis_storage = RedisStorage(redis)

# Logging configuration
logging.basicConfig(level=config.LOGLEVEL, format='%(asctime)s [%(levelname)s] %(message)s')


def create_materialized_view():
    with db_connection.cursor() as cursor:
        logging.info("Creating materialized view")

        # Check if materialized view already exists
        cursor.execute(
            """
            SELECT EXISTS
            (
                SELECT 1
                FROM pg_matviews
                WHERE matviewname = 'user_topic_normalized'
            );
            """
        )
        if cursor.fetchone()[0] and not config.RECREATE_MV:
            logging.info("Materialized view already exists - skipping")
            return

        logging.info("Creating user_topic")
        cursor.execute(
            """
            CREATE MATERIALIZED VIEW user_topic AS
            SELECT ruv.user_id,
                   rt.topic_id,
                   ruv.weight * rt.weight AS weight
            FROM repository_user_visit ruv INNER JOIN repository_topic rt ON ruv.repository_id = rt.repository_id
            WHERE user_id IN (SELECT user_id FROM repository_user_visit GROUP BY user_id HAVING count(*) >= 3);
            """
        )

        logging.info("Creating indexes on user_topic")
        cursor.execute(
            """
            CREATE INDEX idx_ut_user_id
            ON user_topic (user_id);

            CREATE INDEX idx_ut_user_id_topic_id
            ON user_topic (user_id, topic_id);
            """
        )

        logging.info("Creating user_topic_weight_sums")
        cursor.execute(
            """
            CREATE MATERIALIZED VIEW user_topic_weight_sums AS
            SELECT user_id, SUM(weight) AS weight_sum
            FROM user_topic
            GROUP BY user_id;
            """
        )

        logging.info("Creating indexes on user_topic_weight_sums")
        cursor.execute(
            """
            CREATE INDEX idx_utws_user_id
            ON user_topic_weight_sums (user_id);
            """
        )

        logging.info("Creating user_topic_normalized")
        cursor.execute(
            """
            CREATE MATERIALIZED VIEW user_topic_normalized AS
            SELECT ut.user_id, ut.topic_id, SUM((ut.weight) / utws.weight_sum) AS weight
            FROM user_topic ut INNER JOIN user_topic_weight_sums utws ON ut.user_id = utws.user_id
            GROUP BY ut.user_id, ut.topic_id;
            """
        )

        logging.info("Creating indexes on user_topic_normalized")
        cursor.execute(
            """
            CREATE INDEX idx_utn_user_id
            ON user_topic_normalized (user_id);

            CREATE INDEX idx_utn_topic_id
            ON user_topic_normalized (topic_id);
            """
        )

        logging.info("Creating topic_counts")
        cursor.execute(
            """
            CREATE MATERIALIZED VIEW topic_counts AS
            SELECT topic_id, count(*)
            FROM user_topic_normalized
            GROUP BY topic_id
            ORDER BY count(*) DESC;
            """
        )

        logging.info("Creating additional indexes")
        cursor.execute(
            """
            CREATE INDEX idx_ruv_repository_id
            ON repository_user_visit (repository_id);

            CREATE INDEX idx_ruv_user_id
            ON repository_user_visit (user_id);
            """
        )


def get_users():
    logging.info("Getting users")

    # Query user-topic weights
    df_chunks = pd.read_sql_query(
        """
        SELECT *
        FROM user_topic_normalized;
        """,
        con=db_connection_streams,
        chunksize=config.CHUNKSIZE
    )
    df = pd.concat([df for df in df_chunks], ignore_index=True)

    # Add all topic IDs as dummies to the DataFrame to make sure that all of the topics are contained in it and thus
    # the dimensions of the resulting pivot table are always the same
    df_null = pd.read_sql_query(
        """
        SELECT NULL AS user_id, t.id AS topic_id, NULL AS weight
        FROM topic t;
        """,
        con=db_connection,
    )
    df = pd.concat([df, df_null], ignore_index=True)

    # Pivot the DataFrame
    df = df.pivot_table(index='user_id', columns='topic_id', values='weight', fill_value=0, dropna=False)

    return df


def get_repos():
    logging.info("Getting repositories")

    # Make left join with topic to make sure that all topic IDs are in the DataFrame and thus the dimensions of the
    # resulting pivot table are always the same
    df_chunks = pd.read_sql_query(
        """
        SELECT DISTINCT rt.repository_id, t.id AS topic_id, rt.weight
        FROM topic t LEFT JOIN repository_topic rt ON t.id = rt.topic_id;
        """,
        con=db_connection_streams,
        chunksize=config.CHUNKSIZE
    )
    df = pd.concat([df for df in df_chunks], ignore_index=True)

    # Pivot the DataFrame
    df = df.pivot_table(index='repository_id', columns='topic_id', values='weight', fill_value=0, dropna=False)

    return df


def init_hash(name, func):
    logging.info(f"Initializing hash '{name}'")

    if redis_storage.load_hash_configuration(name) is not None:
        if config.RECREATE_CACHE:
            # Clear redis cache if exists as we want to recalculate everything
            redis.flushdb()
        else:
            logging.info(f"'{name}': Already exists - skipping")
            return

    df = func()

    rbp = RandomBinaryProjections(
        name,
        config.LSH_PROJECTION_COUNT,
        rand_seed=config.LSH_RANDOM_SEED
    )

    # Create LSH engine
    lsh_engine = Engine(dim=len(df.columns), lshashes=[rbp], storage=redis_storage)

    # Add vectors to lsh map and store in redis
    logging.info(f"'{name}': Adding vectors to lsh map")
    for index, row in df.iterrows():
        lsh_engine.store_vector(row.values, index)

    # Store hash configuration for later use
    logging.info(f"'{name}': Storing vectors")
    redis_storage.store_hash_configuration(rbp)


def main():
    logging.info("Starting initialization of cache")

    # Create materialized view
    create_materialized_view()

    # Users Hash
    init_hash(config.REDIS_USERS_HASH_NAME, get_users)

    # Repos Hash
    init_hash(config.REDIS_REPOS_HASH_NAME, get_repos)

    # Close connections
    db_connection.close()
    redis.close()

    logging.info("Finished initialization of cache")

    logging.info("Starting web app")
    os.execvp(sys.argv[1], sys.argv[1:])


if __name__ == '__main__':
    main()
