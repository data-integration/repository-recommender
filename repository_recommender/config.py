import os

# General
LOGLEVEL = os.environ.get("RR_LOGLEVEL", "INFO").upper()
CHUNKSIZE = int(os.environ.get("RR_CHUNKSIZE", 100000))
RECREATE_MV = os.getenv("RR_RECREATE_MV", "False") == "True"
RECREATE_CACHE = os.getenv("RR_RECREATE_CACHE", "False") == "True"

# Database
DB_HOST = os.getenv("RR_DB_HOST", "localhost")
DB_PORT = int(os.getenv("RR_DB_PORT", 5432))
DB_USER = os.getenv("RR_DB_USER", "admin")
DB_PASSWORD = os.getenv("RR_DB_PASSWORD", "password")
DB_NAME = os.getenv("RR_DB_NAME", "repository-recommender")
DB_URL = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

# Redis
REDIS_HOST = os.getenv("RR_REDIS_HOST", "localhost")
REDIS_PORT = int(os.getenv("RR_REDIS_PORT", 6379))
REDIS_DB = int(os.getenv("RR_REDIS_DB", 1))
REDIS_USERS_HASH_NAME = 'UsersHash'
REDIS_REPOS_HASH_NAME = 'ReposHash'

# LSH
LSH_PROJECTION_COUNT = int(os.getenv("RR_REDIS_DB", 10))
LSH_RANDOM_SEED = int(os.getenv("RR_REDIS_DB", 1943255))
