import falcon

from repository_recommender.controllers.topics_controller import TopicsController
from .controllers.content_based import ContentBased
from .controllers.collaborative_filtering import CollaborativeFiltering

api = application = falcon.API()

cb = ContentBased()
api.add_route('/cb', cb)

cf = CollaborativeFiltering()
api.add_route('/cf', cf)

topics = TopicsController()
api.add_route('/topics', topics)
