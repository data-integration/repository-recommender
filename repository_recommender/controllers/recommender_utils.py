import logging

import pandas as pd
import psycopg2
import psycopg2.extras
from nearpy import Engine
from nearpy.filters import NearestFilter
from nearpy.hashes import RandomBinaryProjections
from nearpy.storage import RedisStorage
from sklearn.metrics.pairwise import cosine_similarity
import repository_recommender.config as config
from repository_recommender.controllers.known_repo_filter import KnownRepoFilter


class RecommenderUtils:
    def __init__(self):
        # Connect to database
        self.db_connection = psycopg2.connect(
            host=config.DB_HOST,
            port=config.DB_PORT,
            dbname=config.DB_NAME,
            user=config.DB_USER,
            password=config.DB_PASSWORD
        )

        # Connect to redis cache
        import redis as redis
        redis = redis.Redis(
            host=config.REDIS_HOST,
            port=config.REDIS_PORT,
            db=config.REDIS_DB
        )
        self.redis_storage = RedisStorage(redis)
        logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(message)s')

    def get_repo_user_similarities(self, repos, user_feature_vector):
        repos_similarity = {}
        for index, row in repos.iterrows():
            # Equalize indices
            missing_repo_idc = user_feature_vector.index.difference(row.index)
            if missing_repo_idc.size > 0:
                row = row.add(pd.Series([0] * missing_repo_idc.size, index=missing_repo_idc), fill_value=0)

            # Calculate similarity
            repos_similarity[index] = \
                cosine_similarity(user_feature_vector.values.reshape(1, -1), row.values.reshape(1, -1))[0, 0]

        # Sort dictionary by values (=similarity) in descending order
        return sorted(repos_similarity.items(), key=lambda x: x[1], reverse=True)

    def get_user_topic_vector(self, platform_id, external_id):
        user_id = self.get_user_id(platform_id, external_id)

        if user_id is None:
            return None

        logging.info("Getting user feature vector")
        user = pd.read_sql_query(
            """
            SELECT *
            FROM user_topic_normalized utn
            WHERE utn.user_id = {}
            
            UNION ALL
    
            SELECT NULL, t.id, NULL FROM topic t;
            """.format(user_id),
            con=self.db_connection
        )

        if (len(user.index) - user['user_id'].isnull().sum()) == 0:
            return None

        logging.info("Pivoting user feature vector")
        # Assume a weight of 0 if na because the user never showed interest in these topics
        user = user.pivot_table(index='user_id', columns='topic_id', values='weight', fill_value=0, dropna=False)
        return user_id, user.loc[user_id]

    def create_lsh_engine(self, redis_config, known_repos_list=None, no_of_recommendations=None):
        # Get hash config from redis
        hash_config = self.redis_storage.load_hash_configuration(redis_config)

        # Skip if redis is not yet initialized
        if hash_config is None:
            return None

        rbp = RandomBinaryProjections(None, None)
        rbp.apply_config(hash_config)

        # Create LSH engine
        logging.info("Creating LSH engine")

        if known_repos_list is None:
            if no_of_recommendations is None:
                # Dimension = number of topics
                lsh_engine = Engine(dim=327, lshashes=[rbp], storage=self.redis_storage)
            else:
                lsh_engine = Engine(dim=327, lshashes=[rbp],
                                    vector_filters=[NearestFilter(no_of_recommendations)],
                                    storage=self.redis_storage)
        else:
            lsh_engine = Engine(dim=327, lshashes=[rbp],
                                vector_filters=[KnownRepoFilter(known_repos_list),
                                                NearestFilter(no_of_recommendations)],
                                storage=self.redis_storage)
        return lsh_engine

    def get_collaborative_recommendations_for_user(self, platform_id, external_id, user_feature_vector,
                                                   no_of_recommendations=5):
        logging.info("Getting CF-based recommendations")
        # Get LSH engine
        lsh_engine = self.create_lsh_engine(config.REDIS_USERS_HASH_NAME)

        # Get nearest neighbours (users)
        logging.info("Getting nearest neighbours")

        if user_feature_vector is None:
            user_topic_vector = self.get_user_topic_vector(platform_id, external_id)
            if user_topic_vector is None:
                return None

            (user_id, feature_vector) = user_topic_vector
            neighbours = lsh_engine.neighbours(feature_vector.values)

            # Exclude user itself
            for i in range(len(neighbours)):
                if neighbours[i][1] == user_id:
                    neighbours.pop(i)
                    break
        else:
            feature_vector = user_feature_vector
            neighbours = lsh_engine.neighbours(user_feature_vector.values)

        # Load repositories from similar users
        logging.info("Loading repositories")
        repositories = ', '.join(str(int(x[1])) for x in neighbours[1:8])

        if user_feature_vector is None:
            repo_topic_scores = pd.read_sql_query(
                """
                SELECT DISTINCT rt.repository_id, rt.topic_id, rt.weight
                FROM repository_topic as rt JOIN repository_user_visit as ruv ON ruv.repository_id = rt.repository_id
                WHERE ruv.user_id IN ({}) AND rt.repository_id NOT IN (
                    SELECT repository_id FROM repository_user_visit WHERE user_id = {}
                );
                """.format(repositories, user_id),
                con=self.db_connection)
        else:
            repo_topic_scores = pd.read_sql_query(
                """
                SELECT DISTINCT rt.repository_id, rt.topic_id, rt.weight
                FROM repository_topic as rt JOIN repository_user_visit as ruv ON ruv.repository_id = rt.repository_id
                WHERE ruv.user_id IN ({});
                """.format(repositories),
                con=self.db_connection)
        repo_topic_scores = repo_topic_scores.pivot_table(index='repository_id', columns='topic_id', values='weight',
                                                          fill_value=0, dropna=False)

        # Calculate similarity for these repositories and recommend the ones with the best fit
        logging.info("Calculating similarity")

        sorted_repo_similarities = self.get_repo_user_similarities(repo_topic_scores, feature_vector)

        return sorted_repo_similarities[:no_of_recommendations]

    def get_content_based_recommendations_for_user(self, platform_id=None, external_id=None, user_feature_vector=None,
                                                   no_of_recommendations=5):
        logging.info("Getting CB recommendations")
        known_repos_list = None

        if platform_id is not None:
            user_topic_vector = self.get_user_topic_vector(platform_id, external_id)
            if user_topic_vector is None:
                return None

            (user_id, feature_vector) = user_topic_vector
            # Get known repositories of user and ignore them in recommendations
            logging.info("Getting known repositories")
            known_repos = self.get_repos_of_user(user_id)
            known_repos_list = list(sum(known_repos, ()))

        # Get nearest neighbours
        logging.info("Getting nearest neighbours")
        lsh_engine = self.create_lsh_engine(config.REDIS_REPOS_HASH_NAME, known_repos_list, no_of_recommendations)
        if user_feature_vector is None:
            neighbours = lsh_engine.neighbours(feature_vector.values)
        else:
            neighbours = lsh_engine.neighbours(user_feature_vector.values)

        return [(int(x[1]), x[2]) for x in neighbours]

    def get_user_id(self, platform_id, external_id):
        with self.db_connection.cursor(cursor_factory=psycopg2.extras.DictCursor) as cursor:
            cursor.execute(
                """
                SELECT id FROM "user" WHERE platform_id = {} AND external_id = '{}'
                """.format(platform_id, external_id)
            )
            result = cursor.fetchone()
            if result:
                return result['id']

        return None

    def get_repos_of_user(self, user_id):
        with self.db_connection.cursor() as cursor:
            cursor.execute(
                """
                SELECT repository_id FROM repository_user_visit WHERE user_id = {}
                """.format(user_id)
            )
            return cursor.fetchall()

    def get_repository(self, repository_id):
        with self.db_connection.cursor(cursor_factory=psycopg2.extras.DictCursor) as cursor:
            cursor.execute(
                """
                SELECT r.*, u.name AS owner_name, l.name AS language
                FROM (repository r LEFT JOIN "user" u ON r.owner_id = u.id) LEFT JOIN language l on r.language_id = l.id
                WHERE r.id = {}
                """.format(repository_id)
            )
            result = cursor.fetchone()
            if result:
                return result

        return None

    def get_repository_link(self, repository):
        if repository['platform_id'] == 1:
            # GitHub
            link = "https://github.com/%s/%s" % (repository['owner_name'], repository['name'])
        elif repository['platform_id'] == 2:
            # GitLab
            link = "https://gitlab.com/%s/%s" % (repository['owner_name'], repository['name'])
        else:
            # Bitbucket
            link = "https://bitbucket.org/%s/%s" % (repository['owner_name'], repository['name'])
        return link

    def get_popular_topics(self):
        logging.info("Getting most popular topics")
        with self.db_connection.cursor() as cursor:
            cursor.execute(
                """
                SELECT tc.topic_id, t.name FROM topic_counts tc INNER JOIN topic t ON tc.topic_id = t.id ORDER BY tc.count DESC LIMIT 60
                """
            )
            return cursor.fetchall()

    def ratings_to_topic_vector(self, ratings):
        # 327 = number of available topics
        topic_vector = pd.Series([0.0] * 327,
                                 index=range(1, 328))
        for rating in ratings:
            topic_vector[rating["id"]] = rating["rating"]
        return topic_vector
