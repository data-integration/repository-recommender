from nearpy.filters.vectorfilter import VectorFilter


class KnownRepoFilter(VectorFilter):
    """
    Sorts vectors with respect to distance and returns the N nearest.
    """

    def __init__(self, repos):
        """
        Keeps the ids of known repos.
        """
        self.repos = repos

    def filter_vectors(self, input_list):
        """
        Returns subset of specified input list.
        """
        # Return filtered (vector, data, distance )tuple list. Will fail
        # if input is list of (vector, data) tuples.
        filtered_list = [(vector, data, distance) for (vector, data, distance) in input_list if
                         data not in self.repos]
        return filtered_list
