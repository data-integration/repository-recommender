import json
import falcon
import random

from repository_recommender.controllers.recommender_utils import RecommenderUtils


class TopicsController(object):

    def on_get(self, req, resp):
        recommender_utils = RecommenderUtils()
        topics = recommender_utils.get_popular_topics()
        # Exclude general topics: support, library, documentation, api, git, framework, actions
        # awesome
        excluded_topics = [9, 61, 45, 159, 136, 99, 320, 93]
        filtered_topics = [topic for topic in topics if topic[0] not in excluded_topics]
        topics_idx = random.sample(range(0, len(filtered_topics)), 10)
        chosen_topics = [{"id": filtered_topics[i][0], "name": filtered_topics[i][1]} for i in topics_idx]

        resp.body = json.dumps(chosen_topics, ensure_ascii=False)
        resp.status = falcon.HTTP_200
