import json

import falcon

from .recommender_utils import RecommenderUtils


class CollaborativeFiltering(object):

    def on_get(self, req, resp):
        recommender_utils = RecommenderUtils()
        user_ratings = req.get_param("user_ratings")
        user_feature_vector = None
        platform_id = None
        external_user_id = None
        if user_ratings is not None:
            user_ratings = json.loads(user_ratings)
            user_feature_vector = recommender_utils.ratings_to_topic_vector(user_ratings)
        else:
            platform_id = req.get_param("platform_id")
            if platform_id is None:
                doc = {"message": "Parameter 'platform_id' has to be specified."}
                resp.body = json.dumps(doc, ensure_ascii=False)
                resp.status = falcon.HTTP_400
                return

            external_user_id = req.get_param("user_id")
            if external_user_id is None:
                doc = {"message": "Parameter 'user_id' has to be specified."}
                resp.body = json.dumps(doc, ensure_ascii=False)
                resp.status = falcon.HTTP_400
                return

        recommender_utils = RecommenderUtils()
        repositories = recommender_utils.get_collaborative_recommendations_for_user(platform_id, external_user_id,
                                                                                    user_feature_vector)

        if repositories is None:
            doc = {"message": "No recommendations could be found for the provided user."}
            resp.body = json.dumps(doc, ensure_ascii=False)
            resp.status = falcon.HTTP_200
            return

        recommendations = []
        for repository_id, score in repositories:
            repository = recommender_utils.get_repository(repository_id)
            recommendations.append(
                {
                    "id": repository_id,
                    "name": repository['name'],
                    "description": repository['description'],
                    "owner_id": repository['owner_id'],
                    "owner_name": repository['owner_name'],
                    "created": repository['created'].isoformat(),
                    "last_updated": repository['last_updated'].isoformat(),
                    "language": repository['language'],
                    "score": score,
                    "platform_id": repository['platform_id'],
                    "link": recommender_utils.get_repository_link(repository)
                }
            )

        resp.body = json.dumps(recommendations, ensure_ascii=False)
        resp.status = falcon.HTTP_200
