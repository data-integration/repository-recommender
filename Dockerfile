FROM python:3.7-slim

ENV PYTHONUNBUFFERED=1

WORKDIR /code/

COPY requirements.txt .
RUN apt-get update \
    && apt-get install -y --no-install-recommends gcc linux-libc-dev libc6-dev \
    && rm -rf /var/lib/apt/lists/* \
    && pip install --no-cache-dir -r requirements.txt \
    && apt-get purge -y --auto-remove gcc linux-libc-dev libc6-dev

RUN useradd user \
 && chown -R user /code

USER user

COPY . .

EXPOSE 8080

ENTRYPOINT [ "python", "docker_entrypoint.py" ]
CMD [ "gunicorn", "repository_recommender.app", "--bind", "0.0.0.0:8080", "--workers", "3" ]
